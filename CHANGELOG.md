# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3] - 2023/1/13
### Added
- A count of the reserved characters
### Changed
- Starting and ending characters are now stored as strings, not int

## [1.2] - 2023/1/8
### Added
- Better type hints
- `default` parameter to `block()`
- `include_csur` parameter to `block()`
- CSUR support, under `CSUR` variable
### Changed
- License is now MIT
- Updated `.gitlab-ci.yml`

## [1.1] - 2022/10/18
### Added
- Added `BLOCKS_BYNAME` for getting block ranges by name

## [1.0] - 2022/10/16
### Added
- Initial release

[1.3]: https://gitlab.com/whoatemybutter/tinyunicodeblock/-/compare/v1.3.0...master
[1.2]: https://gitlab.com/whoatemybutter/tinyunicodeblock/-/compare/v1.2.0...v1.3.0
[1.1]: https://gitlab.com/whoatemybutter/tinyunicodeblock/-/compare/v1.1.0...v1.2.0
[1.0]: https://gitlab.com/whoatemybutter/tinyunicodeblock/-/compare/v1.0.0...v1.1.0